// ROGERWILCO FRAMEWORK : Anton Boshoff //
 
// ==== INDEX ==== //
// Global Variables
// Sticky Header
// Toggle Navigation
// Expand Navigation
// Collapse Navigation
// Initiate Accordion
// Initiate Tabber
// Equal Heights
// Sidebar Height
// Disable Middle Mouse Click


// ==== NAMESPACE ==== //
window.app = {}


// ==== TRIGGER ON DOCUMENT READY ==== //
jQuery(function($) {

	try {

		app.go.globalVar();
		app.go.toggleNav();
		app.go.initAccordion();
		app.go.initTabber();
		app.go.stickyHeader();
		app.go.disableMiddleMouse();

		// ==== TRIGGER ON RESIZE ( DEBOUNCE ) ==== //
		// function updateResizeDebounce() {

		// }

		// var lazyLayout = _.debounce(updateResizeDebounce, 10);
		// app.go.win.resize(lazyLayout);

		// ==== TRIGGER ON RESIZE ( THROTTLE ) ==== //
		function updateResizeThrottle() {

			// dynamic values
			app.go.winWidth = app.go.win.width();

			if ( app.go.winWidth < 960 ) {
				app.go.isMobile = true;
				app.go.collapseNav();
			} else {
				app.go.isMobile = false;
				app.go.expandNav();
			}

			app.go.equalHeights();
			app.go.sidebarHeight();

		}

		var rThrottled = _.throttle(updateResizeThrottle, 10);
		app.go.win.resize(rThrottled);

		// ==== TRIGGER ON SCROLL ( THROTTLE ) ==== //
		function updateScrollThrottle() {

			// dynamic values
			app.go.winTop = app.go.win.scrollTop();

			app.go.stickyHeader();

		}

		var sThrottled = _.throttle(updateScrollThrottle, 10);
		app.go.win.scroll(sThrottled);

	} catch(err) {

		console.log(err);

	}

});

// ==== TRIGGER AFTER PAGE LOAD ==== //
jQuery(window).load(function(){
	
	app.go.equalHeights();
	app.go.sidebarHeight();

});


// ==== FUNCTIONS ==== //
app.go = {

	// Global Variables
	globalVar: function() {

		// variables
		app.go.win = jQuery(window);
		app.go.body = jQuery('body');
		app.go.nav = jQuery('nav');

		// dynamic values
		app.go.winHeight = app.go.win.height();
		app.go.winWidth = app.go.win.width();
		app.go.winTop = app.go.win.scrollTop();

		if ( app.go.winWidth < 960 ) {
			app.go.isMobile = true;
		} else {
			app.go.isMobile = false;
		}

	},

	// Sticky Header
	stickyHeader: function() {

		var header = jQuery('header#header');

		if ( header.hasClass('header-sticky') ) {

			var elementB = jQuery('.header-sticky-before'),
				elementA = jQuery('.header-sticky-after'),
				heightB = elementB.outerHeight();

			if ( app.go.winTop > heightB ) {
				header.addClass('stick');
				elementA.addClass('header-stick');
			} else {
				header.removeClass('stick');
				elementA.removeClass('header-stick');
			}

		} return false;

	},

	// Toggle Navigation
	toggleNav: function() {

		var toggler = jQuery('.toggler a');

		toggler.on('click', function(e) {
			e.preventDefault();

			if ( app.go.isMobile ) {
				if ( app.go.isActive ) {
					app.go.collapseNav();
				} else {
					app.go.expandNav();
				}
			} return false;
		});

	},

	// Expand Navigation
	expandNav: function() {

		if ( !app.go.isActive ) {
			app.go.nav.show();
		}
		app.go.isActive = true;

	},

	// Collapse Navigation
	collapseNav: function() {

		if ( app.go.isActive ) {
			app.go.nav.hide();
		}
		app.go.isActive = false;

	},

	// Initiate Accordion
	initAccordion: function(){

		// variables
    	var indi = false,
    		accordion = jQuery('.accordion');
    		expand = jQuery('.is-expanded');

    	// if items independant from siblings
    	if ( accordion.hasClass('is-independent') ) {
    		indi = true;
    	}

    	// fade/hide items on initial load
    	jQuery('.itemContents')
      		.css('opacity', 0)
      		.each(function(){
        		var getHeight = jQuery(this).find('.itemContents').height();
        		jQuery(this).find('.itemContents').css('height', getHeight);
      		});
		accordion.find('.itemContents').hide();

		// show items with .is-expanded class
		expand.find('.itemContents')
  			.show()
  			.css('opacity', 1);
		expand.find('.icon').html('<i class="fa fa-chevron-up"></i>');

		// on click
		jQuery('.bar').on('click', function(){
  			var self = jQuery(this);

  			// fade/hide all items
  			if ( indi ) {
  				// do nothing
  			} else {
  				self.parent().parent().children('.is-expanded').find('.itemContents').animate({ opacity: 0 }, function(){
		        	jQuery(this).slideUp(function(){
		          		jQuery(this).parent().removeClass('is-expanded');
		        	});
		      	});
  			}

	      	// expand/show if not already showing
  			if ( self.parent().hasClass('is-expanded') ) {
  				if ( indi ) {
  					self.siblings('.itemContents').animate({ opacity: 0 }, function(){
  						jQuery(this).slideUp();
  						jQuery(this).closest('.accordion>li').removeClass('is-expanded');
	    			});
  				} else {
  					// do nothing
  				}
  			} else {
    			self.siblings('.itemContents').slideDown(function(){
      				jQuery(this).animate({ opacity: 1 });
      				jQuery(this).closest('.accordion>li').addClass('is-expanded');
    			});
  			}
		});

  	},

  	// Initiate Tabber
  	initTabber: function(){

	    jQuery('ul.tabber>li').on('click', function(){
	      	var self = jQuery(this),
	        	dataContent = self.attr('data-contents'),
	          	tabberContent = self.parent('ul').siblings('.tabberContent');
	      	
	      	if (self.parent().hasClass('open')) {
	        	// do nothing
	      	} else {
	        	self.siblings().removeClass('open');
	        	self.addClass('open');

	        	tabberContent.children('li[data-tab!='+dataContent+']').hide();
	        	tabberContent.children('li[data-tab='+dataContent+']').show();
	      	}
	    });

	},

	// Equal Heights
	equalHeights: function() {

		var row = jQuery('.equalize');

		row.each(function() {

			var item = jQuery(this).find('.rw-col');

			if ( app.go.winWidth > 768 ) {

				var tallest = 0;

				item.height('auto');

				item.each(function() {
					var iHeight = jQuery(this).height();

					tallest = tallest > iHeight ? tallest : iHeight;
				});

				item.height(tallest);
			} else {
				item.height('auto');
			}

		});

		jQuery('.pager').find('a').on('click', function() {
			setTimeout(function() {
				app.go.win.trigger('resize');
			}, 600);
		});

	},

	// Sidebar Height
	sidebarHeight: function() {

		var container = jQuery('.rw-container-has-sidebar, .rw-container-has-sidebar-both');

		container.each(function() {

			var side = jQuery(this).find('.rw-sidebar').not('.no-calculate'),
				main = jQuery(this).find('.rw-content').not('.no-calculate');

			side.height('auto');
			main.height('auto');

			if ( app.go.winWidth > 768 ) {

				var tallest = 0,
					sHeight = side.height(),
					mHeight = main.height();

				if ( sHeight > mHeight ) {
					tallest = sHeight;
				} else {
					tallest = mHeight;
				}
				
				side.height(tallest);
				main.height(tallest);

			} else {
				side.height('auto');
				main.height('auto');
			}

		});

	},

	// Disable Middle Mouse Click
	disableMiddleMouse: function() {

		app.go.body.on('mousedown', function(e) {

			if ( e.which == 2 ) {
				e.preventDefault();
			}

		});

	}

}